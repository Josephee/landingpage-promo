// Angular JS {{Principiante}}
var dateIn = document.getElementById("checkin").value;
var dateOut = document.getElementById("checkout").value;

var setting = angular.module('Settings', []);

setting.controller('CtrlSetting', ['$scope', function ($scope) {
    /* RELLENA CON UN 0 */
    $scope.Zero = function (data) { return (data < 10) ? data = '0' + data : data = data; };
    /* GENERADOR DE FECHAS PARA EL BOOKING */
    $scope.FechaBooking = function (Num) {
        var today = new Date();
        today.setMilliseconds(24 * 60 * 60 * 1000);
        (Num === 1) ? today.setMilliseconds(24 * 60 * 60 * 1000) : today = today;
        var dd = today.getDate(), mm = today.getMonth() + 1, yy = today.getFullYear();
        return '' + yy + $scope.Zero(mm) + $scope.Zero(dd);
    };
    $scope.Fechas = {
        FEntrada: $scope.FechaBooking(0),
        FSalida: $scope.FechaBooking(1)
    };
    /* INFORMACIÓN DEL HOTEL */
    $scope.Hotel = {
        hotelName: "Médano Hotel & Suites",
        hotelLocation: "Los Cabos, México",
        url: "http://www.medanocabo.com",
        hotelTel: "01 (624) 143 0309",
        hotelMail: "reservations@medanocabo.com",
        telefono: "016121750860",
        callCenter: "01 (612) 175 0860"
    };
    $scope.Booking = {
        idHotel: 6391,
        parther: "MedanoHotel",
        currency: "MXN"
    };
    $scope.Enlaces = {
        urlOffers: "http://www.medanocabo.com/ofertas-hoteles-en-cabo-san-lucas-mexico/",
        urlSuscription: "http://www.medanocabo.com/recibe-ofertas/",
        urlRooms: "http://www.medanocabo.com/hospedaje-en-cabo-san-lucas/",
        urlReserve: "http://booking.internetpower.com.mx/" + $scope.Booking.parther + "/hotel/hoteldescription.aspx?PropertyNumber=" + $scope.Booking.idHotel + "&Provider=0&Rooms=1&AccessCode=" +
            "&NegociateRate=&Currency=" + $scope.Booking.currency + "&Adults=2&Children=0&CheckIn=" + $scope.Fechas.FEntrada + "&CheckOut=" + $scope.Fechas.FSalida + "&Tab=Rates",
        urlNew: '&Rooms=1&PartnerId=255&StartDay=1&Nights=1&adults=2',
        mejor_tarifa: ""
    };
    $scope.RedSocial = {
        facebook: "https://www.facebook.com/medanocabo",
        twitter: "https://twitter.com/Medano_Hotel",
        google: "",
        youtube: "",
        tripadvisor: "",
        instagram: "https://www.instagram.com/medanocabo/"
    };
    $scope.Tema = {
        colorTheme: "#1f5178",
        colorSupport: "#002658",
        colorDefault: "#FFFFFF",
        urlFont: "https://fonts.googleapis.com/css?family=Quattrocento",
        fontFamily: "'Quattrocento', sans-serif;",
        radius: 0
    };
    /* PAQUETES CON SUS RESPECTIVAS FECHAS */
    $scope.Promo = [{
        nombrePromo: "Paquete 01",
        dias: 1,
        fInicial: "2017/07/01",
        fFinal: "2017/09/30",
        pack: "",
        code: ""
    }, {
        nombrePromo: "Paquete 02",
        dias: 1,
        fInicial: "2017/04/14",
        fFinal: "2017/04/30",
        pack: "",
        code: ""
    }];

    /* REPARTICIÓN DE GAMA DE COLORES EN LA LANDING */
    $scope.colourBrightness = function (hex, percent) {
        hex = hex.replace(/^\s*#|\s*$/g, '');

        if (hex.leng == 3) {
            hex = hex.replace(/(.)/g, '$1$1');
        }

        var r = parseInt(hex.substr(0, 2), 16),
            g = parseInt(hex.substr(2, 2), 16),
            b = parseInt(hex.substr(4, 2), 16);

        r = ((0 | (1 << 8) + r + (256 - r) * percent / 100).toString(16)).substr(1);
        g = ((0 | (1 << 8) + g + (256 - g) * percent / 100).toString(16)).substr(1);
        b = ((0 | (1 << 8) + b + (256 - b) * percent / 100).toString(16)).substr(1);

        return '#' + r + g + b;
    };
    $scope.Colorea = {
        lighttheme: $scope.colourBrightness($scope.Tema.colorTheme, 30.5),
        darktheme: $scope.colourBrightness($scope.Tema.colorTheme, -8.8),
        lightsupport: $scope.colourBrightness($scope.Tema.colorSupport, 30.8),
        darksupport: $scope.colourBrightness($scope.Tema.colorSupport, -8.9),
        lightdefault: $scope.colourBrightness($scope.Tema.colorDefault, 30.6),
        darkdefault: $scope.colourBrightness($scope.Tema.colorDefault, -8.8)
    };
}]);

setting.controller('appController', ['$scope', '$http', '$controller', function ($scope, $http, $controller) {
    /* INYECCIÓN DE DATOS DE LOS CONTROLADORES */
    $controller('CtrlSetting', { $scope: $scope });
    $scope.show = false;
    /* CONFIGURACIÓN  Y VARIABLES */
    var params = {
        propertyNumber: $scope.Booking.idHotel,
        currency: $scope.Booking.currency,
        app: $scope.Booking.parther,
        ext: $scope.Enlaces.urlNew
    },
        _params = 'propertyNumber=' + params.propertyNumber + '&currency=' + params.currency + '&app=' + params.app + params.ext + '&callback=JSON_CALLBACK',
        url = 'http://booking.internetpower.com.mx/DinamicRooms/Request.ashx?' + _params;

    $http.jsonp(url).success(function (resp) {
        setRooms(resp);
    });

    var setRooms = function (json) {
        $scope.Rooms = json.Rooms;
        angular.forEach($scope.Rooms, function (r) {
            if (parseFloat(r.total) > 0) $scope.show = true;
        });
        if (!$scope.show) console.log("Tarifas no disponibles");
    };
}]);

setting.controller('appComparator', ['$scope', '$http', '$controller', function ($, $http, $controller) {
    /* INYECCIÓN DE DATOS DE LOS CONTROLADORES */
    $controller('CtrlSetting', { $scope: $ });
    $controller('appController', { $scope: $ });
    $.show = false;
    /* CONFIGURACIÓN  Y VARIABLES */
    var now = new Date(), d = now.getDate(), m = (now.getMonth() + 1), y = now.getFullYear(),
        params = {
            propertyNumber: $.Booking.idHotel,
            room: ""
        },
        checkIn = "", checkOut = "", _params = "", _url = "";
    now.setMilliseconds(24 * 60 * 60 * 1000);
    checkIn = "" + now.getFullYear() + $.Zero((now.getMonth() + 1)) + $.Zero(now.getDate());
    now.setMilliseconds(24 * 60 * 60 * 1000);
    checkOut = "" + now.getFullYear() + $.Zero((now.getMonth() + 1)) + $.Zero(now.getDate());
    _params = "PropertyNumber=" + params.propertyNumber + "&CheckIn=" + checkIn + "&CheckOut=" + checkOut + "&RoomCode=" + params.room;
    _url = "http://compare.internetpower.com.mx/reports/GetRates?" + _params;

    var params = {
        propertyNumber: $.Booking.idHotel,
        currency: $.Booking.currency,
        app: $.Booking.parther,
        ext: $.Enlaces.urlNew
    },
        _params = 'propertyNumber=' + params.propertyNumber + '&currency=' + params.currency + '&app=' + params.app + params.ext + '&callback=JSON_CALLBACK',
        __url = 'http://booking.internetpower.com.mx/DinamicRooms/Request.ashx?' + _params;
    /* LLAMADA AL SERVIDOR DE DATOS */
    $http.jsonp(__url).success(function (resp) {
        $.Cuarto = resp.Rooms;
        /* LLAMADA AL SERVIDOR DEL COMPARADOR */
        $http.get(_url).then(function (response) {
            setRooms(response.data[0].getRatesModel);
        });
    });

    /* MÉTODO DE OBTENCIÓN DE DATOS */
    var setRooms = function (array) {
        $.Rates = array;
        angular.forEach($.Rates, function (r) {
            if (r.Channel == "InternetPower") {
                r.Channel = "Nosotros";
                let x = 0, y = 0, z = 0;
                angular.forEach($.Cuarto, function (c) {
                    if (parseFloat(c.total.replace(",", "")) != 0 && z == 0) {
                        z = parseFloat(c.total.replace(",", ""));
                        x = y;
                    }
                    if (parseFloat(c.total.replace(",", "") < z)) z = parseFloat(c.total.replace(",", ""));
                    y++;
                });
                r.Rate = $.Cuarto[x].total.replace(",", "");
                r.Rate = parseFloat(r.Rate);
                r.Status = $.Cuarto[x].url;
                if (r.Rate != 0) $.show = true;
            }
            if (r.Channel == "Booking.com") r.Channel = "Booking";
        });
        if (!$.show) console.log("No hay comparador");
    };
}]);

setting.controller('appPromo', ['$scope', '$http', '$controller', function ($, $http, $controller) {
    /* INYECCIÓN DE DATOS DE LOS CONTROLADORES */
    $controller('CtrlSetting', { $scope: $ });
    var cont = 0;
    $.Paquete = [];

    $.fBooking = function (days, dStart, dEnd) {
        var now = new Date();
        (now < Date.parse(dStart)) ? now = new Date(dStart) : now;
        if (days != 0) now.setMilliseconds(days * 24 * 60 * 60 * 1000);
        return '' + now.getFullYear() + $.Zero(now.getMonth() + 1) + $.Zero(now.getDate());
    };

    angular.forEach($.Promo, function (x) {
        $.Paquete[cont] = "http://booking.internetpower.com.mx/" + $.Booking.parther + "/hotel/hoteldescription.aspx?PropertyNumber=" + $.Booking.idHotel + "&Provider=0&Rooms=1&AccessCode=" + x.code +
            "&NegociateRate=&Currency=" + $.Booking.currency + "&Adults=2&Children=0&CheckIn=" + $.fBooking(0, x.fInicial, x.fFinal) + "&CheckOut=" + $.fBooking(x.dias, x.fInicial, x.fFinal) + "&Tab=Rates&packageUv=" + x.pack;
        cont++;
    });
}]);